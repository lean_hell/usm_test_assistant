import { AuthenticationLayout } from "@/layouts/authentication";
import { ErrorPage } from "@/pages/error";
import { HomePage } from "@/pages/home";
import { Item } from "@/pages/tests/[id]";
import { CreateTest } from "@/pages/tests/create";
import { BrowserRouter, Outlet, useRoutes } from "react-router-dom";

function Router() {
  return useRoutes([
    {
      path: "/",
      element: <HomePage />,
    },
    {
      path: "error",
      element: <ErrorPage />,
    },
    {
      path: "tests",
      element: <Outlet />,
      children: [
        {
          path: ":id",
          element: <Item />,
        },
        {
          path: "create",
          element: <CreateTest />,
        },
      ],
    },
  ]);
}

export function RouterProvider() {
  return (
    <BrowserRouter>
      <AuthenticationLayout>
        <Router />
      </AuthenticationLayout>
    </BrowserRouter>
  );
}
