import { API_URL } from "@/libs/api";
import { AuthenticationPage } from "@/pages/authentication";
import ky from "ky";
import { ReactNode, useEffect, useState } from "react";

export function AuthenticationLayout({ children }: { children: ReactNode }) {
  const [isAuth, setIsAuth] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const localPassword = localStorage.getItem("auth");
    ky.post(`${API_URL}/auth`, { body: JSON.stringify({ password: localPassword }) })
      .then(() => {
        setIsAuth(true);
        setLoading(false);
      })
      .catch(() => setLoading(false));
  }, []);

  return !loading ? !isAuth ? <AuthenticationPage setIsAuth={setIsAuth} /> : children : null;
}
