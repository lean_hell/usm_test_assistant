import { RouterProvider } from "@/providers/router";
import "@/styles/globals.css";
import ReactDOM from "react-dom/client";

ReactDOM.createRoot(document.querySelector("main")!).render(<RouterProvider />);
