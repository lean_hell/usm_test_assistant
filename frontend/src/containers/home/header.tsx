export function HeaderSection() {
  const logout = () => {
    localStorage.clear();
    location.reload();
  };

  return (
    <div className="p-2 w-full max-w-4xl flex justify-between items-center bg-neutral-900 rounded-lg gap-2">
      <span className="pl-2 text-indigo-400 tracking-wider text-sm font-bold">USM Test Assistant</span>
      <button onClick={logout} className="px-8 py-1.5 text-sm font-medium tracking-wider bg-rose-300/5 hover:bg-rose-300/10 text-rose-500 rounded duration-200">
        Выйти
      </button>
    </div>
  );
}
