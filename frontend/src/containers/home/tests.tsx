import { API_URL } from "@/libs/api";
import { ITest } from "@/types/test";
import ky from "ky";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

interface IProps {
  search: string;
}

export function TestsSection({ search }: IProps) {
  const [tests, setTests] = useState<ITest[] | null>(null);

  const filteredTests = tests?.filter((test) => test.test_name.includes(search) || test.subject_name.includes(search)) || [];

  useEffect(() => {
    ky.get(`${API_URL}/tests`)
      .json()
      .then((tests) => setTests(tests as ITest[]));
  }, []);

  return (
    <div className="w-full flex flex-col justify-center items-center gap-2">
      {tests ? (
        filteredTests.map((item) => (
          <Link
            key={item.id}
            to={`/tests/${item.id}`}
            className="p-4 w-full max-w-4xl flex flex-col justify-start items-start bg-neutral-900 hover:bg-neutral-800 rounded-lg duration-200"
          >
            <h1 className="font-medium text-lg">{item.subject_name}</h1>
            <p className="text-neutral-400">{item.test_name}</p>
          </Link>
        ))
      ) : (
        <div className="py-8">
          <div className="w-[20px] h-[20px] border-[2px] border-neutral-100/10 border-t-neutral-300 rounded-full animate-spin" />
        </div>
      )}
    </div>
  );
}
