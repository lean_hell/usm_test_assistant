import { API_URL } from "@/libs/api";
import ky from "ky";
import { useRef, useState } from "react";
import { useNavigate } from "react-router-dom";

interface IProps {
  id: string | undefined;
}

export function Upload({ id }: IProps) {
  const [loading, setLoading] = useState(false);
  const fileInputRef = useRef<HTMLInputElement | null>(null);
  const navigate = useNavigate();

  const redirectError = (message: string) => navigate(`/error?message=${message}`);

  const handleButtonClick = () => {
    if (loading) return;
    if (fileInputRef.current) {
      fileInputRef.current.click();
    }
  };

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.files && event.target.files.length > 0) {
      const selectedFile = event.target.files[0];
      setLoading(true);
      ky.post(`${API_URL}/answers/${id}`, { body: selectedFile })
        .then(() => {
          setLoading(false);
          location.reload();
        })
        .catch((error) => {
          switch (error.response.status) {
            case 400:
              redirectError("Необходимо прикрепить файл.");
              break;
            case 406:
              redirectError("Этот файл предназначен для другого теста.");
              break;
          }
        });
    }
  };

  return (
    <>
      <input className="hidden" type="file" ref={fileInputRef} onChange={handleFileChange} accept="text/html" />
      <button onClick={handleButtonClick} className="px-8 py-1.5 text-sm font-medium tracking-wider bg-neutral-100/5 hover:bg-neutral-100/10 rounded duration-200">
        {loading ? <div className="w-[20px] h-[20px] border-[2px] border-neutral-100/10 border-t-neutral-300 rounded-full animate-spin" /> : "Загрузить"}
      </button>
    </>
  );
}
