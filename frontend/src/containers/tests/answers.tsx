import { API_URL } from "@/libs/api";
import { IAnswers } from "@/types/answer";
import ky from "ky";
import { useEffect, useState } from "react";

interface IProps {
  id: string | undefined;
  search: string;
}

export function AnswersSection({ id, search }: IProps) {
  const [answers, setAnswers] = useState<IAnswers[] | null>(null);

  const filteredAnswers = answers?.filter((answer) => answer.question.includes(search)) || [];

  useEffect(() => {
    ky.get(`${API_URL}/answers/${id}`)
      .json()
      .then((answers) => setAnswers((answers as IAnswers[]) || []));
  }, []);

  return (
    <div className="w-full flex flex-col justify-center items-center gap-2">
      {answers ? (
        filteredAnswers.map((item, index) => (
          <div key={index} className="p-4 w-full max-w-4xl flex flex-col justify-start items-start gap-2 bg-neutral-900 rounded-lg">
            <h1 className="font-medium text-lg">{item.question}</h1>
            {Array.isArray(item.answer) ? (
              typeof item.answer[0] === "string" ? (
                <div className="flex gap-8 text-neutral-400">
                  <span>Ответы:</span>
                  <ul className="list-disc">
                    {(item.answer as string[]).map((item, index) => (
                      <li key={index}>{item}</li>
                    ))}
                  </ul>
                </div>
              ) : (
                <div className="flex gap-4 text-neutral-400">
                  <span>Ответы:</span>
                  <table>
                    <tbody>
                      {(item.answer as { title: string; answer: string }[]).map((item, index) => (
                        <tr key={index}>
                          <td className="p-3 border border-neutral-800">{item.title}</td>
                          <td className="p-3 border border-neutral-800">{item.answer}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              )
            ) : (
              <p className="text-neutral-400">Ответ: {item.answer}</p>
            )}

            <div className="pt-2 w-full flex flex-col items-end text-xs text-neutral-50/5">
              <span>{item.author}</span>
              <span>{item.md5}</span>
            </div>
          </div>
        ))
      ) : (
        <div className="py-8">
          <div className="w-[20px] h-[20px] border-[2px] border-neutral-100/10 border-t-neutral-300 rounded-full animate-spin" />
        </div>
      )}
    </div>
  );
}
