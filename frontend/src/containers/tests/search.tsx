import { Dispatch, SetStateAction } from "react";
import { useDebouncedCallback } from "use-debounce";
import { Upload } from "./upload";

interface IProps {
  id: string | undefined;
  setSearch: Dispatch<SetStateAction<string>>;
}

export function SearchSection({ id, setSearch }: IProps) {
  const debounced = useDebouncedCallback((value) => {
    setSearch(value);
  }, 300);

  return (
    <div className="p-2 w-full max-w-4xl flex justify-between items-center bg-neutral-900 rounded-lg gap-2">
      <div className="w-full p-1 flex justify-center items-center bg-neutral-100/5 rounded">
        <svg className="ml-1.5 mr-3" stroke="currentColor" fill="currentColor" strokeWidth="0" viewBox="0 0 512 512" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
          <path d="M456.69 421.39 362.6 327.3a173.81 173.81 0 0 0 34.84-104.58C397.44 126.38 319.06 48 222.72 48S48 126.38 48 222.72s78.38 174.72 174.72 174.72A173.81 173.81 0 0 0 327.3 362.6l94.09 94.09a25 25 0 0 0 35.3-35.3zM97.92 222.72a124.8 124.8 0 1 1 124.8 124.8 124.95 124.95 0 0 1-124.8-124.8z"></path>
        </svg>
        <input
          className="w-full py-0.5 text-sm placeholder:text-neutral-500 bg-transparent outline-none"
          placeholder="Поиск"
          type="text"
          onChange={(e) => debounced(e.target.value)}
        />
      </div>
      <Upload id={id} />
    </div>
  );
}
