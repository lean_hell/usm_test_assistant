import { ITest } from "@/types/test";
import { Link } from "react-router-dom";

export function HeaderSection({ test }: { test: ITest | null }) {
  return (
    <div className="p-2 w-full max-w-4xl flex justify-between items-center bg-neutral-900 rounded-lg gap-2">
      <div className="flex flex-col">
        <span className="pl-2 tracking-wider text-xs font-medium">{test?.subject_name}</span>
        <span className="pl-2 text-neutral-400 tracking-wider text-xs">{test?.test_name}</span>
      </div>
      <Link to="/" className="px-8 py-1.5 text-sm font-medium tracking-wider bg-rose-100/5 hover:bg-rose-100/10 rounded duration-200">
        Назад
      </Link>
    </div>
  );
}
