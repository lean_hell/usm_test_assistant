import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";

export function ErrorPage() {
  const [message, setMessage] = useState("");
  const location = useLocation();
  const navigate = useNavigate();

  useEffect(() => {
    const queryParams = new URLSearchParams(location.search);
    const value = queryParams.get("message");
    if (value) {
      setMessage(value);
    } else {
      setMessage("Неизвестная ошибка");
    }
  }, []);

  return (
    <div className="fixed w-full h-full flex flex-col justify-center items-center gap-2">
      <div className="w-[24rem] flex flex-col justify-center items-center gap-2">
        <h1 className="text-xl font-medium">Ошибка</h1>
        <p className="w-full px-3 py-1.5 flex flex-col justify-center items-center bg-neutral-900 rounded outline-none text-center text-rose-500">{message}</p>
        <button onClick={() => navigate(-1)} className="w-full px-3 py-1 font-medium bg-neutral-100 text-neutral-950 hover:bg-neutral-400 rounded duration-200">
          Назад
        </button>
      </div>
    </div>
  );
}
