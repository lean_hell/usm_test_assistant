import { AnswersSection } from "@/containers/tests/answers";
import { HeaderSection } from "@/containers/tests/header";
import { SearchSection } from "@/containers/tests/search";
import { API_URL } from "@/libs/api";
import { ITest } from "@/types/test";
import ky from "ky";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

export function Item() {
  const [search, setSearch] = useState("");
  const [test, setTest] = useState<ITest | null>(null);
  const navigate = useNavigate();

  const { id } = useParams();

  useEffect(() => {
    ky.get(`${API_URL}/tests/${id}`)
      .json()
      .then((test) => setTest(test as ITest))
      .catch(() => navigate("/"));
  }, []);

  return (
    <div className="p-4 w-full flex flex-col justify-center items-center gap-2">
      <HeaderSection test={test} />
      <SearchSection id={id} setSearch={setSearch} />
      <AnswersSection id={id} search={search} />
    </div>
  );
}
