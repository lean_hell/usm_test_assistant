import { API_URL } from "@/libs/api";
import ky from "ky";
import { useState } from "react";
import { Link } from "react-router-dom";

export function CreateTest() {
  const [error, setError] = useState("");

  const formSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (event.target instanceof HTMLFormElement) {
      const subject_name = event.target[0];
      const test_name = event.target[1];
      if (subject_name instanceof HTMLInputElement && test_name instanceof HTMLInputElement) {
        if (subject_name.value === "" || test_name.value === "") return;
        ky.post(`${API_URL}/tests`, { body: JSON.stringify({ subject_name: subject_name.value, test_name: test_name.value }) })
          .then(() => location.replace("/"))
          .catch((error) => {
            switch (error.response.status) {
              case 409:
                setError("Уже существует");
                break;
              case 500:
                setError("Серверная ошибка, обратитесь к администратору ресурса");
                break;
            }
          });
      }
    }
  };

  return (
    <div className="fixed w-full h-full flex flex-col justify-center items-center gap-2">
      <form className="w-[24rem] flex flex-col justify-center items-center gap-2" onSubmit={formSubmit}>
        <input className="w-full px-3 py-1.5 bg-neutral-900 placeholder:text-neutral-500 rounded outline-none" type="text" placeholder="Название предмета" />
        <input className="w-full px-3 py-1.5 bg-neutral-900 placeholder:text-neutral-500 rounded outline-none" type="text" placeholder="Название теста" />
        <div className="w-full flex gap-2">
          <button className="w-full px-3 py-1 font-medium bg-neutral-100 text-neutral-950 hover:bg-neutral-400 rounded duration-200">Создать</button>
          <Link to="/" className="w-full px-3 py-1 text-center font-medium bg-neutral-100 text-neutral-950 hover:bg-neutral-400 rounded duration-200">
            Назад
          </Link>
        </div>
      </form>
      <p className="text-rose-500 text-sm font-medium">{error}</p>
    </div>
  );
}
