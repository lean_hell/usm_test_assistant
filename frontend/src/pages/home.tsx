import { HeaderSection } from "@/containers/home/header";
import { SearchSection } from "@/containers/home/search";
import { TestsSection } from "@/containers/home/tests";
import { useState } from "react";

export function HomePage() {
  const [search, setSearch] = useState("");

  return (
    <div className="p-4 w-full flex flex-col justify-center items-center gap-2">
      <HeaderSection />
      <SearchSection setSearch={setSearch} />
      <TestsSection search={search} />
    </div>
  );
}
