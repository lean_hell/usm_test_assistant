import { API_URL } from "@/libs/api";
import ky from "ky";
import { Dispatch, SetStateAction, useState } from "react";

interface IProps {
  setIsAuth: Dispatch<SetStateAction<boolean>>;
}

export function AuthenticationPage({ setIsAuth }: IProps) {
  const [error, setError] = useState("");

  const formSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (event.target instanceof HTMLFormElement) {
      const inputElement = event.target[0];
      if (inputElement instanceof HTMLInputElement) {
        if (inputElement.value === "") return;
        ky.post(`${API_URL}/auth`, { body: JSON.stringify({ password: inputElement.value }) })
          .then(() => {
            setIsAuth(true);
            localStorage.setItem("auth", inputElement.value);
          })
          .catch((error) => {
            inputElement.value = "";
            switch (error.response.status) {
              case 401:
                setError("Неверный пароль");
                break;
              case 500:
                setError("Серверная ошибка, обратитесь к администратору ресурса");
                break;
            }
          });
      }
    }
  };

  return (
    <div className="fixed w-full h-full flex flex-col justify-center items-center gap-2">
      <form className="w-[18rem] flex flex-col justify-center items-center gap-2" onSubmit={formSubmit}>
        <input className="w-full px-3 py-1.5 bg-neutral-900 rounded outline-none" type="text" />
        <button className="w-full px-3 py-1 font-medium bg-neutral-100 text-neutral-950 hover:bg-neutral-400 rounded duration-200">Войти</button>
      </form>
      <p className="text-rose-500 text-sm font-medium">{error}</p>
    </div>
  );
}
