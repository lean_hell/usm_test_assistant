export interface IAnswers {
  question: string;
  answer: string | string[] | { title: string; answer: string }[];
  author: string;
  md5: string;
}
