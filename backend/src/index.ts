import cors from "cors";
import "dotenv/config";
import * as HyperExpress from "hyper-express";

import { AnswersRoute } from "./routes/answers/route";
import { AuthRoute } from "./routes/auth/route";
import { TestsRoute } from "./routes/tests/route";

const server = new HyperExpress.Server();

server.use(
  cors({
    origin: ["https://uta.carzi.ru", "http://localhost:5173"],
    credentials: true,
  })
);

server.use("/auth", AuthRoute);
server.use("/tests", TestsRoute);
server.use("/answers", AnswersRoute);

server
  .listen(3000)
  .then(() => console.log("[ HTTP ] Started on http://localhost:3000."))
  .catch(console.log);
