import { MiddlewareHandler } from "hyper-express";

export const verify: MiddlewareHandler = async (request, response) => {
  try {
    const { password } = await request.json();
    if (!password) response.status(400).end();

    if (password === process.env.ACCESS_PASSWORD) {
      return response.status(200).end();
    } else {
      return response.status(401).end();
    }
  } catch (error) {
    return response.status(500).end();
  }
};
