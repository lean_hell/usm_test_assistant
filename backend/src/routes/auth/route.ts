import { Router } from "hyper-express";
import { verify } from "./controllers/verify";

const router = new Router();
router.post("/", verify);

export const AuthRoute = router;
