import { Router } from "hyper-express";
import { create } from "./controllers/create";
import { findAll } from "./controllers/findAll";
import { findOne } from "./controllers/findOne";

const router = new Router();
router.get("/", findAll);
router.get("/:id", findOne);
router.post("/", create);

export const TestsRoute = router;
