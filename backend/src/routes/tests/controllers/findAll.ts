import { Database } from "../../../database";
import { MiddlewareHandler } from "hyper-express";

export const findAll: MiddlewareHandler = async (request, response) => {
  try {
    const tests = await Database.Tests.findAll();
    return response.status(200).json(tests);
  } catch (error) {
    return response.status(500).end();
  }
};
