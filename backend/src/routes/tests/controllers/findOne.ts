import { MiddlewareHandler } from "hyper-express";
import { Database } from "../../../database";

export const findOne: MiddlewareHandler = async (request, response) => {
  try {
    const { id } = request.params;
    const test = await Database.Tests.findOne({ where: { id } });
    if (test) {
      return response.status(200).json(test);
    } else {
      return response.status(404).end();
    }
  } catch (error) {
    return response.status(500).end();
  }
};
