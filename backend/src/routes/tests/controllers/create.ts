import { Database } from "../../../database";
import { MiddlewareHandler } from "hyper-express";

export const create: MiddlewareHandler = async (request, response) => {
  try {
    const { subject_name, test_name } = await request.json();
    if (!subject_name || !test_name) response.status(400).end();

    const isExist = await Database.Tests.findOne({ where: { subject_name, test_name } });
    if (isExist) return response.status(409).end();

    await Database.Tests.create({ subject_name, test_name });

    return response.status(200).end();
  } catch (error) {
    return response.status(500).end();
  }
};
