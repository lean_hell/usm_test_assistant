import { Router } from "hyper-express";
import { findAll } from "./controllers/findAll";
import { upload } from "./controllers/upload";

const router = new Router();
router.get("/:id", findAll);
router.post("/:id", upload);

export const AnswersRoute = router;
