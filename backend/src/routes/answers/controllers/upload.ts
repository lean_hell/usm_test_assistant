import { load } from "cheerio";
import { createHash } from "crypto";
import fs from "fs";
import { MiddlewareHandler } from "hyper-express";
import { Database } from "../../../database";

export const upload: MiddlewareHandler = async (request, response) => {
  try {
    const { id } = request.params;

    const test = await Database.Tests.findOne({ where: { id } }).then((date) => date?.dataValues);
    if (!test) return response.status(404).end();

    let answers = await Database.Answers.findOne({ where: { test_id: test.id } }).then((date) => date?.dataValues);
    answers = answers?.answers ? JSON.parse(answers.answers) : [];

    const validAnswers = new Map();
    answers.forEach((item: any) => {
      const { question } = item;
      validAnswers.set(question, { ...item });
    });

    const body = await request.buffer();
    if (body.length <= 0) return response.status(400).end();
    const $ = load(body);

    const title = $("head title").text();
    if (!title.includes(test.test_name)) return response.status(406).end();

    const hash = createHash("md5").update(body).digest("hex");
    const author = $("div.logininfo").text().split(" ").slice(4).join("_");
    const selectedDivs = $("div.que");

    selectedDivs.each((_, element) => {
      const scores = $(element).find("div.grade").text().split(" ");
      if (scores[1] !== scores[3]) return;

      const question = $(element).find("div.qtext").text();
      if (validAnswers.has(question)) return;

      // Если это radio ответ
      const radioElement = $(element).find('input[type="radio"]:checked').first();
      const isRadio = radioElement.siblings("label.ml-1").text() || radioElement.siblings(".d-flex").find(".flex-fill.ml-1").text();

      // Если это checkbox ответ
      const isCheckbox = $(element)
        .find('input[type="checkbox"]:checked')
        .map((_, element) => $(element).siblings(".d-flex").find(".flex-fill.ml-1").text())
        .get();

      // Если это select ответ
      const selectElements = $(element)
        .find("table.answer tbody tr")
        .map((_, element) => {
          const title = $(element).find("td.text").text();
          const answer = $(element).find("select option:selected").text();

          return { title, answer };
        })
        .get();
      const isSelected = selectElements[0] && selectElements;

      // Если это text ответ
      const isText = $(element).find("span.answer input").val();

      validAnswers.set(question, { answer: isText || isRadio || isSelected || isCheckbox || null, md5: hash, author });
    });

    if (answers[0]) {
      await Database.Answers.update({ answers: JSON.stringify([...validAnswers].map(([key, value]) => ({ question: key, ...value }))) }, { where: { test_id: id } });
    } else {
      await Database.Answers.create({ test_id: id, answers: JSON.stringify([...validAnswers].map(([key, value]) => ({ question: key, ...value }))) });
    }

    if (!fs.existsSync("./database/files")) fs.mkdirSync("./database/files");
    fs.writeFileSync(`./database/files/${author}-${hash}.html`, body);

    return response.status(200).end();
  } catch (error) {
    console.log(error);
    return response.status(500).end();
  }
};
