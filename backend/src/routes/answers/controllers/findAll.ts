import { MiddlewareHandler } from "hyper-express";
import { Database } from "../../../database";

export const findAll: MiddlewareHandler = async (request, response) => {
  try {
    const { id } = request.params;

    const answers = await Database.Answers.findOne({ where: { test_id: id } });
    return response.status(200).json(JSON.parse(answers?.dataValues.answers || "[]"));
  } catch (error) {
    return response.status(500).end();
  }
};
