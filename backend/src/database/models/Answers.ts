import { DataTypes } from "sequelize";
import { Database } from "../connect";

export const Answers = Database.define("Answers", {
  test_id: DataTypes.INTEGER,
  answers: DataTypes.TEXT,
});
