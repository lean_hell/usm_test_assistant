import { DataTypes } from "sequelize";
import { Database } from "../connect";

export const Tests = Database.define("Tests", {
  subject_name: DataTypes.TEXT,
  test_name: DataTypes.TEXT,
});
