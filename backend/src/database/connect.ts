import { Sequelize } from "sequelize";

export const Database = new Sequelize({
  dialect: "sqlite",
  storage: "./database/database.sqlite",
  logging: false,
});

try {
  Database.authenticate().then(() => Database.sync({ alter: true }));
  console.log("[ SQLITE ] Подключение установлено.");
} catch (error) {
  console.log("[ SQLITE ] Не удалось подключится к серверу.", error);
  process.exit();
}
