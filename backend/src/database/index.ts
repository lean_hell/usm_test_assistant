import { Answers } from "./models/Answers";
import { Tests } from "./models/Tests";

export const Database = { Answers, Tests };
